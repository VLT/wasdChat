import io from 'socket.io-client';
import {computed, ref, watchEffect} from 'vue'
import {useGetChannelInfo} from "./wasdAPI.js";
import _ from "lodash";

export const useWASDChat = (channelName, wasdToken) => {
    const socket = io("wss://chat.wasd.tv", {
        autoConnect: false,
        transports: ["websocket"],
        timeout: 40000,
        pingInterval: 25000,
        pingTimeout: 5000,
    })

    const {
        chatTokenReady,
        streamInfoReady,
        channelInfo,
        chatToken
    } = useGetChannelInfo(channelName, wasdToken)

    const streamId = computed(() => _.get(channelInfo.value, 'media_container.media_container_streams[0].stream_id', 0))
    const channelId = computed(() => _.get(channelInfo.value, 'channel.channel_id', 0))
    const connected = ref(false)

    watchEffect(() => {
        if(chatTokenReady.value && streamInfoReady.value) {
            socket.connect()
            if(connected.value) {
                socket.emit("join", {
                    streamId: streamId.value,
                    channelId: channelId.value,
                    jwt: chatToken.value,
                    excludeStickers: true,
                });
            }
        }
    })

    socket.on("joined", (data) => {

    });

    socket.on("connect", () => {
        connected.value = socket.connected
    });

    socket.on('disconnect', () => {
        connected.value = socket.connected
    });

    return {connected,socket}
}